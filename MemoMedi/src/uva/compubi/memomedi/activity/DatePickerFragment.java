package uva.compubi.memomedi.activity;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Fragmento de di�logo para escoger una fecha.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class DatePickerFragment extends DialogFragment {

	/**
	 * Escuchador establecido
	 */
	private OnDateSetListener listener;

	/**
	 * No se permite crear de esta manera.
	 */
	@SuppressWarnings("unused")
	private DatePickerFragment() {

	}

	/**
	 * Constructor b�sico.
	 * 
	 * @param listener
	 *            Escuchador.
	 */
	public DatePickerFragment(final OnDateSetListener listener) {
		this.listener = listener;
	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();

		// Create a new instance of TimePickerDialog and return it
		return new DatePickerDialog(getActivity(), this.listener,
				c.get(Calendar.YEAR), c.get(Calendar.MONTH),
				c.get(Calendar.DAY_OF_MONTH));
	}

}