package uva.compubi.memomedi.activity;

import java.util.ArrayList;

import uva.compubi.memomedi.R;
import uva.compubi.memomedi.model.Consumption;
import uva.compubi.memomedi.model.Product;
import uva.compubi.memomedi.model.ProductPersistence;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.os.Vibrator;
import android.provider.CalendarContract;
import android.provider.CalendarContract.CalendarAlerts;
import android.support.v4.app.NotificationCompat;

/**
 * Recibe los eventos y alarmas del calendario del dispositivo.
 * 
 * En el caso de que sean de la propia apliación lanza una nueva actividad para
 * escanear el producto corresspondiente.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class NotificationReceiver extends BroadcastReceiver {

	/**
	 * Devuelve el producto asociado al evento indicado, comprobando si ha sido
	 * creado por la aplicación, comparando el calendario al que está asociado y
	 * su identificador de evento.
	 * 
	 * Además especifica la variable {@link consumptionIndex} como el índice de
	 * la toma del producto si existiera.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param calendarId
	 *            Identificador del calendario del evento.
	 * @param eventId
	 *            Identificador del evento.
	 * @return Producto asociado al identificador del calendario y del evento
	 *         pasados por parámetro o null si no se encontró el producto (no ha
	 *         sido creado por la aplicación).
	 */
	private Product isApplicationEvent(final Context context,
			final long calendarId, final long eventId) {
		final ArrayList<Product> products = new ProductPersistence()
				.getAllProducts(context);
		Product productAux = null;
		boolean found = false;
		if (products != null) {
			ArrayList<Consumption> events;			
			final int productsSize_1 = products.size() - 1;
			int consumptionsSize_1;

			Consumption consumptionAux;
			for (int i = productsSize_1; !found && i >= 0; i--) {
				productAux = products.get(i);
				if (productAux.getCalendarId() == calendarId) {
					events = productAux.getEvents();
					consumptionsSize_1 = events.size() - 1;
					for (int j = consumptionsSize_1; !found && j >= 0; j--) {
						consumptionAux = events.get(j);
						if (consumptionAux.getEventID() == eventId) {
							productAux.setEventIndex(j);
							found = true;
						}
					}
				}
			}
		}
		return found ? productAux : null;
	}

	@Override
	public void onReceive(final Context context, final Intent intent) {

		final String ACTION = intent.getAction();

		if (ACTION != null
				&& ACTION.equals("android.intent.action.EVENT_REMINDER")) {

			// Comprobar que sea de la aplicación (ID del calendario y del
			// evento)
			final Cursor c = context.getContentResolver().query(
					CalendarContract.CalendarAlerts.CONTENT_URI_BY_INSTANCE,
					new String[] { CalendarAlerts.EVENT_ID,
							CalendarAlerts.CALENDAR_ID },
					CalendarContract.CalendarAlerts.ALARM_TIME + "=?",
					new String[] { intent.getData().getLastPathSegment() },
					null);

			final byte NO_ID = -1;
			long eventID = NO_ID;
			long calendarID = NO_ID;
			if (c.moveToNext()) {
				// Get the field values
				eventID = c.getLong(0);
				calendarID = c.getLong(1);
			}

			if (eventID != NO_ID && calendarID != NO_ID) {
				// && ) {
				final Product mProduct = isApplicationEvent(context,
						calendarID, eventID);
				if (mProduct != null) {
					showNotification(mProduct, context);
				}
			}
		}
	}

	/**
	 * Muestra una notificación de la consumición indicada.
	 * 
	 * @param consumption
	 *            Consumición a mostrar.
	 * @param context
	 *            Contexto de uso.
	 */
	private void showNotification(final Product product, final Context context) {

		final Intent launchIntent = new Intent(context.getApplicationContext(),
				ScanModeActivity.class);		
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		launchIntent.putExtra(ScanModeActivity.PRODUCT_KEY, product);		

		// Clave para identificar la notificación
		final int notificationKey = product.getCode().hashCode()
				+ product.getEventIndex();

		final NotificationCompat.Builder builder = new NotificationCompat.Builder(
				context);		
		final String title = String.format(
				context.getString(R.string.notification_message), product
						.getEvents().get(product.getEventIndex())
						.getConsumptionNumber(), product.getCode());
		final String text = (product.getName() != null ? product.getName()
				+ " - " : "")
				+ context.getString(R.string.notification_message_hint);		
		builder.setContentIntent(
				PendingIntent.getActivity(context, notificationKey,
						launchIntent, 0))
				.setSmallIcon(android.R.drawable.ic_dialog_alert)
				.setTicker(title + ' ' + text).setAutoCancel(true)
				.setWhen(System.currentTimeMillis()).setContentTitle(title)
				.setContentText(text)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
		final Notification noti = builder.build();

		// Sonido de la notificación
		try {
			noti.sound = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		} catch (final Exception e) {
		}

		// Parpadeo durante 400 ms y luego este apagado durante 1 segundo.
		try {
			noti.flags |= Notification.FLAG_SHOW_LIGHTS;
			try {
				// Morado
				noti.ledARGB = 0xFF00FFFF;
			} catch (final Exception e) {
				// Azul
				noti.ledARGB = 0xFF0000FF;
			}
			noti.ledOnMS = 400;
			noti.ledOffMS = 1000;
		} catch (final Exception e) {
		}
		
		// Vibrar si el dispositivo lo admite
		try {
			((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
			.vibrate(new long[] { 0, 100, 200, 300, 400, 500, 0}, -1);
		} catch (final Exception e) {
		}

		// Mostrar la notificación
		try {
			((NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE)).notify(
					notificationKey, noti);
		} catch (final Exception ex) {
		}
	}
}