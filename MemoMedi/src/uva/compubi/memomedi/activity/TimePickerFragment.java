package uva.compubi.memomedi.activity;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Fragmento de di�logo para escoger una fecha.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class TimePickerFragment extends DialogFragment {

	/**
	 * Escuchador establecido
	 */
	private OnTimeSetListener listener;

	/**
	 * No se permite crear de esta manera.
	 */
	@SuppressWarnings("unused")
	private TimePickerFragment() {

	}

	/**
	 * Constructor b�sico.
	 * 
	 * @param listener
	 *            Escuchador.
	 */
	public TimePickerFragment(final OnTimeSetListener listener) {
		this.listener = listener;
	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this.listener,
				c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
	}

}