package uva.compubi.memomedi.activity;

import uva.compubi.memomedi.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

/**
 * Actividad inicial de la aplicaci�n.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class SplashActivity extends Activity {

	@Override
	public void onCreate(final Bundle saved) {
		// Establecer el layout
		super.onCreate(saved);
		
		// Pantalla completa
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.setContentView(R.layout.layout_splash);

		// Girar el icono del Splash
		final long initialTime = System.currentTimeMillis();
		giraSplash();
		
		// Lanzar el men�
		final long finalTime = 1500 - (System.currentTimeMillis() - initialTime);
		new Handler().postDelayed(new Runnable() {
			@Override
			public final void run() {
				CommonFunctions.startActivityDeletePrevious(SplashActivity.this, MenuActivity.class);
			}
		// Si aun queda tiempo para los 1800 ms, se espera, si no, lanza la nueva activity-
		}, finalTime > 0 ? finalTime : 1);
	}

	/**
	 * Gira la imagen de la UI de 0 a 90 grados en 0.4 segundos.
	 */
	private void giraSplash() {
		final short TIEMPO = 700;
		final float DESDE = 0f;
		final float HASTA = 90f;
		final int HACIA = Animation.RELATIVE_TO_SELF;
		final float PIVOT = 0.5f;

		final RotateAnimation anim1 = new RotateAnimation(DESDE, HASTA, HACIA,
				PIVOT, HACIA, PIVOT);
		anim1.setInterpolator(new LinearInterpolator());
		anim1.setDuration(TIEMPO);

		final RotateAnimation anim2 = new RotateAnimation(HASTA, DESDE, HACIA,
				PIVOT, HACIA, PIVOT);
		anim2.setInterpolator(new LinearInterpolator());
		anim2.setDuration(TIEMPO);

		final ImageView splash = (ImageView) findViewById(R.id.image_splash);
		anim1.setAnimationListener(new AnimationListener() {

			public void onAnimationEnd(final Animation animation) {
				splash.startAnimation(anim2);
			}

			public void onAnimationRepeat(final Animation animation) {

			}

			public void onAnimationStart(final Animation animation) {

			}
		});
		anim2.setAnimationListener(new AnimationListener() {

			public void onAnimationEnd(final Animation animation) {
				splash.startAnimation(anim1);
			}

			public void onAnimationRepeat(final Animation animation) {

			}

			public void onAnimationStart(final Animation animation) {
			}
		});

		splash.startAnimation(anim1);
	}
}