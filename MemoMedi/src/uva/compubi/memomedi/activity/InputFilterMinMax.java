package uva.compubi.memomedi.activity;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Establece un filtro de un m�nimo y m�ximo.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class InputFilterMinMax implements InputFilter {

	/**
	 * M�nimo del filtro.
	 */
	private int min;
	/**
	 * M�ximo del filtro.
	 */
	private int max;

	/**
	 * Establece un filtro con unos valores m�nimo y m�ximo.
	 * 
	 * @param min
	 *            M�nimo del filtro.
	 * @param max
	 *            M�ximo del filtro.
	 */
	public InputFilterMinMax(final int min, final int max) {
		this.min = min;
		this.max = max;
	}

	@Override
	public CharSequence filter(final CharSequence source, final int start,
			final int end, final Spanned dest, final int dstart, final int dend) {
		try {
			int input = Integer.parseInt(dest.toString() + source.toString());
			if (isInRange(this.min, this.max, input))
				return null;
		} catch (NumberFormatException nfe) {
		}
		return "";
	}

	/**
	 * Comprueba que est� en el rango.
	 * 
	 * @param a
	 *            Valor m�nimo.
	 * @param b
	 *            Valor m�ximo.
	 * @param c
	 *            Valor a comprobar que est� enrte el m�nimo y el m�ximo.
	 * @return True si el valor es menor o igual que el m�ximo y mayor o igual
	 *         que el m�nimo.
	 */
	private boolean isInRange(final int a, final int b, final int c) {
		return b > a ? c >= a && c <= b : c >= b && c <= a;
	}
}