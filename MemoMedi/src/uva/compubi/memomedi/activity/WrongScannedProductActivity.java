package uva.compubi.memomedi.activity;

import uva.compubi.memomedi.R;
import uva.compubi.memomedi.model.Product;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Actividad inicial de la aplicaci�n.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class WrongScannedProductActivity extends Activity implements
		OnClickListener {

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_wrong_scanned_product);

		// Establecer escuchadores de clics
		this.findViewById(R.id.wrong_scanned_product_photo_uri_id)
				.setOnClickListener(this);
		this.findViewById(R.id.wrong_scanned_product_cancel_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.wrong_scanned_product_try_again_button_id)
				.setOnClickListener(this);

		// Muestra los datos del producto par aque el usuario pueda recordarlo
		// con mayor facilidad
		this.mProduct = (Product) this.getIntent().getSerializableExtra(
				PRODUCT_KEY);
		showProdcutData();
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.wrong_scanned_product_photo_uri_id:
			CommonFunctions.showPictureDevice(this, ((Button) v).getText()
					.toString());
			break;
		case R.id.wrong_scanned_product_cancel_button_id:
			goToMenu();
			break;
		case R.id.wrong_scanned_product_try_again_button_id:
			goToScanMode();
			break;
		}
	}

	/**
	 * Clave para indentificar el producto
	 */
	static String PRODUCT_KEY = "product_key";
	/**
	 * Producto que debe ser escaneado.
	 */
	private Product mProduct;

	/**
	 * Muestra los datos del producto para que el usuario pueda recordarlo con
	 * mayor facilidad.
	 */
	private void showProdcutData() {
		if (this.mProduct != null) {

			// Mostrar imagen
			final String optionalUri = this.mProduct.getUri();
			final Button mButton = ((Button) this
					.findViewById(R.id.wrong_scanned_product_photo_uri_id));
			if (optionalUri != null && optionalUri.length() > 0) {
				mButton.setText(optionalUri);
				mButton.setVisibility(View.VISIBLE);
			} else {
				mButton.setVisibility(View.GONE);
			}

			// Mostrar c�digo y nombre (si estuviera establecido)
			final String optionalName = this.mProduct.getName();
			final TextView hintText = ((TextView) this
					.findViewById(R.id.wrong_scanned_product_subtitle_id));
			hintText.setText(String.format(
					this.getString(R.string.wrong_scanned_product_subtitle_explain),
					this.mProduct.getCode()
							+ (optionalName != null
									&& optionalName.length() > 0 ? " - "
									+ optionalName : "")));
		}
	}

	/**
	 * Vuelve al men� principal.
	 */
	private void goToMenu() {
		CommonFunctions.startActivityDeletePrevious(this, MenuActivity.class);
	}

	/**
	 * Lanza el esc�ner de productos.
	 */
	private void goToScanMode() {
		CommonFunctions.startActivityExtraDeletePrevious(this,
				ScanModeActivity.class, ScanModeActivity.PRODUCT_KEY,
				this.mProduct);
	}
}