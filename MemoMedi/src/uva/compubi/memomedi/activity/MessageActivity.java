package uva.compubi.memomedi.activity;

import uva.compubi.memomedi.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

/**
 * Muestra un mensaje al usuario y vuelve al men� principal.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class MessageActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_message);

		// Mostrar los elementos adecuado por pantalla
		createElements();

		// Comenzar el contador de tiempo
		changeCounter(3, true);
	}

	/**
	 * Clave para las mostrar el tipo de mensaje adecuado seg�n se indique.
	 */
	final static String MESSAGE_LAYOUT_KEY = "key_message_layout";
	/**
	 * Bandera que indica que se va a mostrar el mensaje como
	 * "producto a�adido correctamente".
	 */
	final static byte MESSAGE_LAYOUT_ADDED_RIGHT = 0;
	/**
	 * Bandera que indica que se va a mostrar el mensaje como
	 * "producto escaneado correctamente".
	 */
	final static byte MESSAGE_LAYOUT_SCANNED_RIGHT = 1;
	/**
	 * Bandera que indica que se va a mostrar el mensaje por defecto.
	 */
	final static byte MESSAGE_LAYOUT_DEFAULT_RIGHT = MESSAGE_LAYOUT_ADDED_RIGHT;

	/**
	 * Crea los elementos del layout convenientemente. Existen dos posibles
	 * layouts derivados: de a�adir un nuevo producto o de escanear un producto
	 * en una toma.
	 */
	private void createElements() {
		final byte OPCION_MOSTRAR = this.getIntent().getByteExtra(
				MessageActivity.MESSAGE_LAYOUT_KEY,
				MessageActivity.MESSAGE_LAYOUT_DEFAULT_RIGHT);

		((TextView) this.findViewById(R.id.message_title_id))
				.setText(OPCION_MOSTRAR == MessageActivity.MESSAGE_LAYOUT_ADDED_RIGHT ? R.string.message_title_added_right
						: R.string.message_title_scanned_right);

		final TextView mSubtitle = (TextView) this
				.findViewById(R.id.message_subtitle_id);
		if (OPCION_MOSTRAR == MessageActivity.MESSAGE_LAYOUT_ADDED_RIGHT) {
			mSubtitle.setVisibility(View.GONE);
		} else {
			if (OPCION_MOSTRAR == MessageActivity.MESSAGE_LAYOUT_SCANNED_RIGHT) {
				mSubtitle.setText(R.string.message_subtitle_explain);
				mSubtitle.setVisibility(View.VISIBLE);
			}
		}
	}

	/**
	 * Muestra el contador din�mico en el mensaje de retorno al men� principal.
	 * 
	 * @param current
	 *            Contador actual a mostrar.
	 * @param updateNow
	 *            Si es True se aplicar�n los cambios instant�neamente. Si es
	 *            False en 1 segundo.
	 */
	private void changeCounter(final int current, final boolean updateNow) {
		final TextView mMessageBack = (TextView) this
				.findViewById(R.id.message_back_message_id);
		new Handler().postDelayed(new Runnable() {
			@Override
			public final void run() {
				if (current == 0) {
					finish();
				} else {
					mMessageBack.setText(String.format(MessageActivity.this
							.getString(R.string.message_back_message), current));
					changeCounter(current - 1, false);
				}
			}
		}, updateNow ? 0 : 1000);
	}

	@Override
	/**
	 * No se permite volver atr�s con el bot�n de retorno del dispositivo para evitar estados inconsistentes.
	 */
	public void onBackPressed() {
	}
}