package uva.compubi.memomedi.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.json.JSONObject;

import uva.compubi.memomedi.R;
import uva.compubi.memomedi.api.HelperAPI;
import uva.compubi.memomedi.model.Consumption;
import uva.compubi.memomedi.model.Product;
import uva.compubi.memomedi.model.ProductPersistence;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * Muesta el formulario para a�adir un nuevo producto y sus tomas en el
 * calendario.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class AddProductActivity extends FragmentActivity implements
		OnClickListener, OnDateSetListener, OnTimeSetListener {

	@Override
	public void onCreate(final Bundle savedInstanceState) {

		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_add_product);

		// Establecer el valor por defecto y limitar entradas de texto
		final EditText consumptionsNumberView = (EditText) findViewById(R.id.add_product_consumptions_number_id);
		consumptionsNumberView
				.setFilters(new InputFilter[] { new InputFilterMinMax(
						Product.MIN_CONSUMPTIONS_NUMBER,
						Product.MAX_CONSUMPTIONS_NUMBER) });
		consumptionsNumberView.setText("" + Product.MIN_CONSUMPTIONS_NUMBER);

		final EditText frequencyHoursView = (EditText) findViewById(R.id.add_product_frequency_hour_number_id);
		frequencyHoursView
				.setFilters(new InputFilter[] { new InputFilterMinMax(
						Product.MIN_FREQUENCY_HOURS,
						Product.MAX_FREQUENCY_HOURS) });
		frequencyHoursView.setText("" + 24);

		final EditText frequencyMinsView = (EditText) findViewById(R.id.add_product_frequency_min_number_id);
		frequencyMinsView.setFilters(new InputFilter[] { new InputFilterMinMax(
				Product.MIN_FREQUENCY_MIN, Product.MAX_FREQUENCY_MIN) });
		frequencyMinsView.setText("" + Product.MIN_FREQUENCY_MIN);

		// Establecer la fecha actual y tres minutos m�s por defecto (s�lo
		// inicialmente para que no coincidan con la actual hora y no se muestre
		// al usuario la notificaci�n de la toma)
		final Calendar mDate = Calendar.getInstance();
		mDate.add(Calendar.MINUTE, 3);
		((Button) AddProductActivity.this
				.findViewById(R.id.add_product_start_date_button_id))
				.setText(dateSDF.format(mDate.getTime()));
		((Button) AddProductActivity.this
				.findViewById(R.id.add_product_start_hour_button_id))
				.setText(hourSDF.format(mDate.getTime()));

		// Mostrar los calendarios del usuario
		setCalendarSpinner();

		// Establecer escuchadores de clics
		this.findViewById(R.id.add_product_left_arrow_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_scan_product_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_optional_photo_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_optional_photo_uri_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_optional_photo_delete_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_start_date_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_start_hour_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.add_product_save_add_button_id)
				.setOnClickListener(this);
	}

	/**
	 * Muestra los calendarios disponibles para a�adir las consumiciones.
	 */
	private void setCalendarSpinner() {

		// Mostrar lista de calendarios
		final Cursor cur = getContentResolver().query(Calendars.CONTENT_URI,
				new String[] { BaseColumns._ID, // 0
						Calendars.ACCOUNT_NAME, // 1
						Calendars.CALENDAR_DISPLAY_NAME, // 2
				// Calendars.OWNER_ACCOUNT // 3
				}, "", null, null);

		// Usar el cursor para iterar sobre los calendarios
		final ArrayList<SpinnerCalendar> calendars = new ArrayList<SpinnerCalendar>();
		while (cur.moveToNext()) {
			calendars
					.add(new SpinnerCalendar(cur.getLong(0), cur.getString(2)));
		}

		// Se a�ade la lista de calendarios al spinner
		final SpinnerAdapter adapter = new ArrayAdapter<SpinnerCalendar>(this,
				R.layout.spinner_item, calendars);
		((Spinner) findViewById(R.id.add_product_calendar_spinner_id))
				.setAdapter(adapter);
	}

	/**
	 * Tipo de datos de un elemento en la lista del Spinner.
	 * 
	 * @author Cristian TG
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class SpinnerCalendar {
		/**
		 * Id del calendario.
		 */
		long id;
		/**
		 * Texto a mostrar referente al calendario.
		 */
		private String name;

		/**
		 * Constructor deun elemento del spinner.
		 * 
		 * @param id
		 *            Identificador del calendario.
		 * @param name
		 *            Nombre del calendario.
		 */
		public SpinnerCalendar(final long id, final String name) {
			this.id = id;
			this.name = name;
		}

		@Override
		public String toString() {
			return this.name;
		}
	}

	/**
	 * Mensaje por defecto cuando no hay establecida la URI de la imagen.
	 */
	private final static int DEFAULT_NO_URI_SG = R.string.add_product_optional_photo_def_text;

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {

		case R.id.add_product_scan_product_button_id:
			// Lanzar un intent para obtener un c�digo de barras (o QR)
			startActivityForResult(new Intent(
					"com.google.zxing.client.android.SCAN"), KEY_SCAN);
			break;

		case R.id.add_product_left_arrow_button_id:
			// Volver al men� principal
			goToMenu();
			break;

		case R.id.add_product_optional_photo_uri_id:
			// Mustra la imagen seleccionada
			CommonFunctions.showPictureDevice(this, ((Button) v).getText()
					.toString());
			break;

		case R.id.add_product_optional_photo_button_id:
			// 1. Im�genes ya existentes
			final Intent pickIntent = new Intent();
			pickIntent.setType("image/*");
			pickIntent.setAction(Intent.ACTION_GET_CONTENT);

			// 2. Imagenes sin hacer
			final Intent takePhotoIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);

			// 3. Se crea el escogedor con ambas posibilidades
			final Intent chooserIntent = Intent.createChooser(pickIntent,
					this.getString(R.string.add_product_optional_photo_msg));
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
					new Intent[] { takePhotoIntent });

			// 4. Se lanza la actividad para obtener la ruta
			startActivityForResult(chooserIntent, KEY_PHOTO);
			break;

		case R.id.add_product_optional_photo_delete_button_id:
			// Descarta y oculta la imagen seleccionada
			final Button optionalPhotoUri = (Button) this
					.findViewById(R.id.add_product_optional_photo_uri_id);
			optionalPhotoUri.setText(this.getString(DEFAULT_NO_URI_SG));
			optionalPhotoUri.setVisibility(View.GONE);
			v.setVisibility(View.GONE);
			// Muestra de nuevo el bot�n de elecci�n de imagen
			this.findViewById(R.id.add_product_optional_photo_button_id)
					.setVisibility(View.VISIBLE);
			break;

		case R.id.add_product_start_date_button_id:
			// Muestra un selector de fechas
			new DatePickerFragment(this).show(getSupportFragmentManager(),
					"date_picker");
			break;

		case R.id.add_product_start_hour_button_id:
			// Muestra un selector de horas y minutos
			new TimePickerFragment(this).show(getSupportFragmentManager(),
					"time_picker");
			break;

		case R.id.add_product_save_add_button_id:
			v.setEnabled(false);
			// Intena a�adir el producto si todos sus campos son correctos
			this.findViewById(R.id.add_product_progress_bar).setVisibility(
					View.VISIBLE);
			addProduct();
			break;
		}
	}

	/**
	 * Clave para intercambiar la ruta de la imagen.
	 */
	private static final byte KEY_PHOTO = 123;

	/**
	 * Clave para intercambiar el resultado del c�digo le�do.
	 */
	private static final byte KEY_SCAN = 125;

	/**
	 * Formateador y parseador de fechas.
	 */
	@SuppressLint("SimpleDateFormat")
	private static final SimpleDateFormat dateSDF = new SimpleDateFormat(
			"dd/MM/yy");

	/**
	 * Formateador y parseador de horas.
	 */
	@SuppressLint("SimpleDateFormat")
	private static final SimpleDateFormat hourSDF = new SimpleDateFormat(
			"HH:mm");

	@Override
	public void onDateSet(final DatePicker view, final int selectedYear,
			final int selectedMonth, final int selectedDay) {

		// Establecer una fecha igual o posterior a la actual
		final Date currentDate = Calendar.getInstance().getTime();

		// Fecha (a�o, mes, d�a, hora y minutos)
		final Calendar selectedDate = Calendar.getInstance();
		selectedDate.setTime(new GregorianCalendar(selectedYear, selectedMonth,
				selectedDay).getTime());
		Date selectedTimeDate = new Date();
		try {
			selectedTimeDate = hourSDF.parse(((Button) AddProductActivity.this
					.findViewById(R.id.add_product_start_hour_button_id))
					.getText().toString());
		} catch (final ParseException e) {
		}
		final Calendar selectedTime = Calendar.getInstance();
		selectedTime.setTime(selectedTimeDate);
		selectedDate.set(Calendar.HOUR_OF_DAY,
				selectedTime.get(Calendar.HOUR_OF_DAY));
		selectedDate.set(Calendar.MINUTE, selectedTime.get(Calendar.MINUTE));

		final boolean correctChange = selectedDate.getTime().after(currentDate);
		((Button) AddProductActivity.this
				.findViewById(R.id.add_product_start_date_button_id))
				.setText(dateSDF.format(correctChange ? selectedDate.getTime()
						: currentDate));
		((Button) AddProductActivity.this
				.findViewById(R.id.add_product_start_hour_button_id))
				.setText(hourSDF.format(correctChange ? selectedDate.getTime()
						: currentDate));
	}

	@Override
	public void onTimeSet(final TimePicker view, final int hourOfDay,
			final int minute) {

		final Calendar currentDate = Calendar.getInstance();

		// Se comprueba que la fecha total no sea menor que la fecha actual
		Date dateDate = currentDate.getTime();
		try {
			dateDate = dateSDF
					.parse(((Button) findViewById(R.id.add_product_start_date_button_id))
							.getText().toString());
		} catch (final ParseException e) {
		}

		final Calendar mCalendar = Calendar.getInstance();
		mCalendar.setTime(dateDate);
		mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
		mCalendar.set(Calendar.MINUTE, minute);

		((Button) AddProductActivity.this
				.findViewById(R.id.add_product_start_hour_button_id))
				.setText(hourSDF.format(currentDate.after(mCalendar) ? currentDate
						.getTime() : mCalendar.getTime()));
	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent intent) {

		// Existen datos que procesar
		if (intent != null) {

			final Uri uri = intent.getData();

			// 1. Proviene de elecci�n de ruta de la imagen
			if (requestCode == KEY_PHOTO && uri != null) {

				try {
					// Se ha elegido una imagen
					final Cursor cursor = getContentResolver().query(uri,
							new String[] { MediaColumns.DATA }, null, null,
							null);
					cursor.moveToFirst();

					// Mostrar la ruta de la imagen al usuario
					final Button optionalPhotoUri = (Button) this
							.findViewById(R.id.add_product_optional_photo_uri_id);
					optionalPhotoUri.setText(cursor.getString(0));
					optionalPhotoUri.setVisibility(View.VISIBLE);
					this.findViewById(R.id.add_product_optional_photo_button_id)
							.setVisibility(View.GONE);
					this.findViewById(
							R.id.add_product_optional_photo_delete_button_id)
							.setVisibility(View.VISIBLE);
					cursor.close();
				} catch (final Exception ex) {

				}

			} else {

				// 2. Proviene de un escaneo de c�digo de barras (o QR)
				if (requestCode == KEY_SCAN && resultCode == RESULT_OK) {
					setNewCode(intent);
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, intent);
	}

	/**
	 * Establece el c�digo escenado en el layout.
	 * 
	 * @param intent
	 *            Resultado del escaneo.
	 */
	private void setNewCode(final Intent intent) {
		final String contents = intent.getStringExtra("SCAN_RESULT");
		if (contents != null) {
			((EditText) this.findViewById(R.id.add_product_code_number_id))
					.setText(contents);

			// Lanza en segundo plano la b�squeda del nombre
			new FindNameAPI().execute(contents);

		}
	}

	/**
	 * Pide informaci�n del nombre del producto a la API Rest de la aplicaci�n.
	 * 
	 * @author Cristian TG
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class FindNameAPI extends
			AsyncTask<String, Void, StringBuilder> {

		@Override
		protected StringBuilder doInBackground(final String... params) {
			return HelperAPI.getNameAPI(params[0]);
		}

		@Override
		protected void onPostExecute(final StringBuilder result) {
			// Mostrar mensaje al usuario si se ha guardado el producto
			// correctamente
			String name;
			try {
				name = new JSONObject(result.toString()).getString("name");
				if (name != null && name.length() > 0) {
					((EditText) findViewById(R.id.add_product_optional_name_id))
							.setText(name);
				}
			} catch (final Exception ex) {

			}
		}
	}

	/**
	 * Gestiona todos los elementos necesarios para guardar el producto
	 */
	private void addProduct() {

		// 1. Verificar todos los campos del producto
		boolean nextElement = true;
		final String errorMessage = this
				.getString(R.string.add_product_mandatory_hint);

		// 1.1. C�digo del producto
		final EditText codeEditText = (EditText) findViewById(R.id.add_product_code_number_id);
		final String code = codeEditText.getText().toString();
		if (code != null && code.length() < Product.MIN_CODE_LENGTH) {
			codeEditText.setError(errorMessage);
			codeEditText.requestFocus();
			nextElement = false;
		}

		// 1.2. Mombre opcional del producto
		final String optionalName = ((EditText) findViewById(R.id.add_product_optional_name_id))
				.getText().toString();

		// 1.3. Uri opcional de la imagen
		String optionalUri = ((Button) findViewById(R.id.add_product_optional_photo_uri_id))
				.getText().toString();
		optionalUri = optionalUri.equals(this.getString(DEFAULT_NO_URI_SG)) ? ""
				: optionalUri;

		// 1.4. N�mero de consumiciones
		int totalConsumptions = Product.MIN_CONSUMPTIONS_NUMBER;
		final EditText consumptionsNumberEditText = (EditText) findViewById(R.id.add_product_consumptions_number_id);
		try {
			totalConsumptions = Integer.parseInt(consumptionsNumberEditText
					.getText().toString());
			if (totalConsumptions < Product.MIN_CONSUMPTIONS_NUMBER
					|| totalConsumptions > Product.MAX_CONSUMPTIONS_NUMBER) {
				consumptionsNumberEditText.setError(errorMessage);
			}
		} catch (final Exception ex) {
			consumptionsNumberEditText.setError(errorMessage);
			consumptionsNumberEditText.requestFocus();
			nextElement = false;
		}

		// 1.5. Frecuencia (horas)
		int hourFrequency = Product.MIN_FREQUENCY_HOURS;
		final EditText hoursEditText = (EditText) findViewById(R.id.add_product_frequency_hour_number_id);
		try {
			hourFrequency = Integer
					.parseInt(hoursEditText.getText().toString());
		} catch (final Exception ex) {
			hoursEditText.setError(errorMessage);
			hoursEditText.requestFocus();
			nextElement = false;
		}

		// 1.6. Frecuencia (minutos)
		int minFrequency = Product.MIN_FREQUENCY_MIN;
		final EditText minsEditText = (EditText) findViewById(R.id.add_product_frequency_min_number_id);
		try {
			minFrequency = Integer.parseInt(minsEditText.getText().toString());
		} catch (final Exception ex) {
			minsEditText.setError(errorMessage);
			minsEditText.requestFocus();
			nextElement = false;
		}

		// 1.6.1. Frecuencia final (horas y minutos)
		final int frequency = hourFrequency * 60 + minFrequency;
		if (!Product.rightFrequency(frequency)) {
			hoursEditText.setError(errorMessage);
			minsEditText.setError(errorMessage);
			nextElement = false;
		}

		// 1.7. Comienzo (d�a,mes y a�o)
		Date dateDate = null;
		try {
			dateDate = dateSDF
					.parse(((Button) findViewById(R.id.add_product_start_date_button_id))
							.getText().toString());
		} catch (ParseException e) {
			nextElement = false;
		}

		// 1.8. Comienzo (hora y minutos)
		Date hourDate = null;
		try {
			hourDate = hourSDF
					.parse(((Button) findViewById(R.id.add_product_start_hour_button_id))
							.getText().toString());
		} catch (ParseException e) {
			nextElement = false;
		}

		// 1.9. Calendario donde indicar las tomas
		final long calendarId = ((SpinnerCalendar) ((Spinner) findViewById(R.id.add_product_calendar_spinner_id))
				.getSelectedItem()).id;

		// 2. Insertar eventos si lso campos fueron especificados correctamente
		if (nextElement) {

			// 2.1. Fecha de comienzo
			final Calendar startDate = Calendar.getInstance();
			startDate.setTime(dateDate);

			final Calendar timeCalendar = Calendar.getInstance();
			timeCalendar.setTime(hourDate);

			startDate.set(Calendar.HOUR_OF_DAY,
					timeCalendar.get(Calendar.HOUR_OF_DAY));
			startDate.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
			startDate.set(Calendar.SECOND, 0);

			this.consumptionsNumber = totalConsumptions;
			final Product mProduct = new Product(code, optionalName,
					optionalUri, frequency, startDate, calendarId);

			new AddProductHandler().execute(mProduct);
		} else {
			this.findViewById(R.id.add_product_save_add_button_id).setEnabled(
					true);
			this.findViewById(R.id.add_product_progress_bar).setVisibility(
					View.GONE);
		}
	}

	/**
	 * N�mero de tomas indicadas para la creaci�n del producto.
	 */
	private int consumptionsNumber = 0;

	/**
	 * Gestiona la creaci�n de eventos en el calendario en segundo plano.
	 * 
	 * @author Cristian TG
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class AddProductHandler extends
			AsyncTask<Product, Void, Boolean> {

		@Override
		protected Boolean doInBackground(final Product... params) {
			try {

				final Product mProduct = params[0];
				// 2.2. Lista de eventos (con alarma)
				final ArrayList<Consumption> events = new ArrayList<Consumption>();
				Calendar consumptionDate = (Calendar) mProduct.getStartDate()
						.clone();
				final String code = mProduct.getCode();
				final int frequency = mProduct.getFrequency();
				final long calendarId = mProduct.getCalendarId();
				long dateMillis;
				ContentResolver cr;
				ContentValues values;
				final String eventTitle = AddProductActivity.this
						.getString(R.string.add_product_consumption);
				long eventID;
				Uri uri;
				for (int i = 0; i < AddProductActivity.this.consumptionsNumber; i++) {
					cr = getContentResolver();
					dateMillis = consumptionDate.getTimeInMillis();
					values = new ContentValues();
					values.put(Events.DTSTART, dateMillis);
					values.put(Events.DTEND, dateMillis);
					values.put(Events.TITLE,
							String.format(eventTitle, i + 1, code));
					values.put(Events.CALENDAR_ID, calendarId);
					values.put(Events.EVENT_TIMEZONE, "Europe/Madrid");
					uri = cr.insert(Events.CONTENT_URI, values);

					eventID = Long.parseLong(uri.getLastPathSegment());

					cr = getContentResolver();
					values = new ContentValues();
					values.put(Reminders.MINUTES, 0);
					values.put(Reminders.EVENT_ID, eventID);
					values.put(Reminders.METHOD, Reminders.METHOD_ALERT);
					cr.insert(Reminders.CONTENT_URI, values);

					events.add(new Consumption(code, eventID, false,
							consumptionDate, i + 1));
					consumptionDate = (Calendar) consumptionDate.clone();
					consumptionDate.add(Calendar.MINUTE, frequency);
				}
				mProduct.setEvents(events);

				// 2. Guardar el producto si se ha podido a�adir al calendario
				if (new ProductPersistence().saveProduct(
						AddProductActivity.this, mProduct)) {
					// 3. Notificar al usuario
					return true;
				}
			} catch (final Exception ex) {

			}
			return false;
		}

		@Override
		protected void onPostExecute(final Boolean isInserted) {
			// 3. Mostrar mensaje al usuario si se ha guardado el producto
			// correctamente
			if (isInserted != null && isInserted) {
				CommonFunctions.startActivityExtraDeletePrevious(
						AddProductActivity.this, MessageActivity.class,
						MessageActivity.MESSAGE_LAYOUT_KEY,
						MessageActivity.MESSAGE_LAYOUT_ADDED_RIGHT);
			} else {
				Toast.makeText(AddProductActivity.this,
						R.string.add_product_error_saving, Toast.LENGTH_SHORT)
						.show();
				AddProductActivity.this.findViewById(
						R.id.add_product_progress_bar).setVisibility(View.GONE);
				AddProductActivity.this.findViewById(
						R.id.add_product_save_add_button_id).setEnabled(true);
			}
		}
	}

	/**
	 * Vuelve al men� principal.
	 */
	private void goToMenu() {
		CommonFunctions.startActivityDeletePrevious(this, MenuActivity.class);
	}

}