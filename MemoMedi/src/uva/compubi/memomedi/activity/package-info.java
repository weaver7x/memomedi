/**
 * Contiene todas la l�gica de negocio de las pantallas (Activity) de la aplicaci�n junto a sus clases relacionadas.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package uva.compubi.memomedi.activity;