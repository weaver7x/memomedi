package uva.compubi.memomedi.activity;

import java.util.Date;

import uva.compubi.memomedi.R;
import uva.compubi.memomedi.model.Consumption;
import uva.compubi.memomedi.model.Product;
import uva.compubi.memomedi.model.ProductPersistence;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract.Events;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * Muestra las opciones posibles para realizar la toma del producto:
 * 
 * 1. Escanear el producto.
 * 
 * 2. Saltarse el escaneo y asumir que se ha tomado el producto correcto.
 * 
 * 3. Cancelar e ir al men� principal de la aplicaci�n.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ScanModeActivity extends Activity implements OnClickListener {

	/**
	 * Clave para indentificar el producto
	 */
	static String PRODUCT_KEY = "product_key";

	/**
	 * Clave para intercambiar el resultado del c�digo le�do.
	 */
	private static final byte KEY_SCAN = 125;

	/**
	 * Producto que debe ser escaneado.
	 */
	private Product mProduct;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_scan_mode);

		// Mostrar datos del producto
		final TextView title = (TextView) this
				.findViewById(R.id.scan_mode_title);

		final Intent mIntent = getIntent();
		this.mProduct = (Product) mIntent.getSerializableExtra(PRODUCT_KEY);
		final String name = this.mProduct.getName();
		title.setText(String.format(getString(R.string.notification_message),
				this.mProduct.getEvents().get(this.mProduct.getEventIndex())
						.getConsumptionNumber(), this.mProduct.getCode()
						+ (name != null ? '\n' + name : "")));

		// Establecer escuchadores de clics
		this.findViewById(R.id.scan_mode_scan_product_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.scan_mode_skip_scan_button_id)
				.setOnClickListener(this);
		this.findViewById(R.id.scan_mode_cancel_button_id).setOnClickListener(
				this);

	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.scan_mode_scan_product_button_id:
			startActivityForResult(new Intent(
					"com.google.zxing.client.android.SCAN"), KEY_SCAN);
			break;
		case R.id.scan_mode_skip_scan_button_id:
			rightConsumption();
			break;
		case R.id.scan_mode_cancel_button_id:
			goToMenu();
			break;
		}
	}

	/**
	 * Vuelve al men� principal.
	 */
	private void goToMenu() {
		CommonFunctions.startActivityDeletePrevious(this, MenuActivity.class);
	}

	/**
	 * Comprueba si el producto escenado es correcto.
	 */
	private void checkConsumption(final Intent intent) {
		final String contents = intent.getStringExtra("SCAN_RESULT");
		if (this.mProduct.getCode().equals(contents)) {

			rightConsumption();
		} else {
			CommonFunctions.startActivityExtraDeletePrevious(this,
					WrongScannedProductActivity.class,
					WrongScannedProductActivity.PRODUCT_KEY, this.mProduct);
		}

	}

	/**
	 * Anota la toma en el calendario.
	 * 
	 * Muestra la pantalla al usuario de producto escaneado correcto.
	 */
	private void rightConsumption() {

		// Edita el evento del calendario indicando qeu se ha realizado la toma
		// correctamente.
		final ContentValues values = new ContentValues();

		// Cambiamos el t�tulo y el contenido
		final Consumption consumption = this.mProduct.getEvents().get(
				this.mProduct.getEventIndex());
		final long eventID = consumption.getEventID();
		final String code = this.mProduct.getCode();
		values.put(Events.TITLE, String.format(
				this.getString(R.string.add_product_consumption),
				consumption.getConsumptionNumber(),
				code + " ["
						+ this.getString(R.string.scan_mode_consumption_done)
						+ ']'));
		values.put(Events.DESCRIPTION,
				this.getString(R.string.scan_mode_consumption_done) + ": "
						+ Product.dateFormat.format(new Date()));
		getContentResolver().update(
				ContentUris.withAppendedId(Events.CONTENT_URI, eventID),
				values, null, null);

		// Guardar en persistencia
		if (new ProductPersistence().saveEventDone(this, code,
				this.mProduct.getCalendarId(), eventID)) {

			// Lanza la actividad que muestra la toma correcta
			CommonFunctions.startActivityExtraDeletePrevious(this,
					MessageActivity.class, MessageActivity.MESSAGE_LAYOUT_KEY,
					MessageActivity.MESSAGE_LAYOUT_SCANNED_RIGHT);
		}
	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent intent) {
		// Hay datos que procesar
		if (intent != null) {
			// Proviene de un escaneo de c�digo de barras (o QR)
			if (requestCode == KEY_SCAN && resultCode == RESULT_OK) {
				checkConsumption(intent);
			}
		}
		super.onActivityResult(requestCode, resultCode, intent);
	}
}