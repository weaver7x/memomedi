package uva.compubi.memomedi.activity;

import java.io.Serializable;

import uva.compubi.memomedi.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * Funciones comunes a las actividades.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class CommonFunctions {

	/**
	 * Modo de lanzar actividades, manteniendo en la pila la actividad
	 * lanzadora.
	 */
	private static final byte START_ACTIVITY_NO_DELETE = 0;
	/**
	 * Modo de lanzar actividades, no manteniendo en la pila la actividad
	 * lanzadora.
	 */
	private static final byte START_ACTIVITY_DELETE_PREVIOUS = 1;

	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	static void startActivityDeletePrevious(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions
				.newActivityDeletePrevious(fromActivity, toActivityClass);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	static void startActivity(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.newActivity(fromActivity, toActivityClass);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora
	 * con un objeto asociado.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	static <T> void startActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value) {
		CommonFunctions.newActivityExtra(fromActivity, toClass, key, value);
	}

	/**
	 * Lanza una actividad, no manteniendo en la pila la actividad lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	static <T> void startActivityExtraDeletePrevious(
			final Activity fromActivity, final Class<?> toClass,
			final String key, final T value) {
		CommonFunctions.newActivityExtraDeletePrevious(fromActivity, toClass,
				key, value);
	}	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	private static void newActivityDeletePrevious(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.executeActivity(fromActivity, new Intent(fromActivity,
				toActivityClass), START_ACTIVITY_DELETE_PREVIOUS);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	private static void newActivity(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.executeActivity(fromActivity, new Intent(fromActivity,
				toActivityClass), START_ACTIVITY_NO_DELETE);
	}

	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	private static <T> void newActivityExtraDeletePrevious(
			final Activity fromActivity, final Class<?> toClass,
			final String key, final T value) {
		CommonFunctions.executeActivityExtra(fromActivity, toClass, key, value,
				START_ACTIVITY_DELETE_PREVIOUS);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * con un objeto asociado serializable.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	private static <T> void newActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value) {
		CommonFunctions.executeActivityExtra(fromActivity, toClass, key, value,
				START_ACTIVITY_NO_DELETE);
	}

	/**
	 * Lanza una actividad segun el modo especificado. Si ocurre algun error se
	 * muestra un mensaje de error por UI.
	 * 
	 * @param activity
	 *            Actividad del contexto lanzadora.
	 * @param intent
	 *            Intencion que contiene el destino del lanzamiento.
	 * @param mode
	 *            Indica si se mantiene en la pila de anteriores actividades o
	 *            no.
	 */
	private static void executeActivity(final Activity activity,
			final Intent intent, final byte mode) {
		try {
			if (mode == START_ACTIVITY_DELETE_PREVIOUS) {
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				activity.finish();
			}
			activity.startActivity(intent);
		} catch (final Exception e) {
			// TODO: Mostrar mensaje indicando que hubo un error
		}
	}

	/**
	 * Lanza una actividad segun el modo especificado con un objeto asociado
	 * serializable.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 * @param mode
	 *            Indica si se mantiene en la pila de anteriores actividades o
	 *            no.
	 */
	private static <T> void executeActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value,
			final byte mode) {
		final Intent intent = new Intent(fromActivity, toClass);

		try {
			intent.putExtra(key, (Serializable) value);
			CommonFunctions.executeActivity(fromActivity, intent, mode);
		} catch (final Exception e) {
			try {
				intent.putExtra(key, "" + value);
				CommonFunctions.executeActivity(fromActivity, intent, mode);
			} catch (final Exception f) {

			}
		}
	}

	/**
	 * Lanza una intenci�n impl�cita de visualizar una imagen del dispositivo.
	 * Si la ruta esta vac�a, es nula o es el mensaje por defecto no hace nada.
	 * 
	 * @param act
	 *            Actividad del contexto.
	 * @param uri
	 *            Ruta de la imagen a visualizar.
	 */
	public static void showPictureDevice(final Activity act, final String uri) {
		if (uri != null
				&& !uri.isEmpty()
				&& uri.compareTo(act
						.getString(R.string.add_product_optional_photo_def_text)) != 0) {
			final Intent intent = new Intent();
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse("file://" + uri), "image/*");
			CommonFunctions.newActivity(act, intent);
		}
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param mIntent
	 *            Intencion personalizada.
	 */
	private static void newActivity(final Activity fromActivity,
			final Intent mIntent) {
		CommonFunctions.executeActivity(fromActivity, mIntent,
				START_ACTIVITY_NO_DELETE);
	}
}