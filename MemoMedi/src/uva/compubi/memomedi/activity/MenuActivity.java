package uva.compubi.memomedi.activity;

import java.util.ArrayList;
import java.util.Calendar;

import uva.compubi.memomedi.R;
import uva.compubi.memomedi.model.Consumption;
import uva.compubi.memomedi.model.Product;
import uva.compubi.memomedi.model.ProductPersistence;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * Actividad inicial de la aplicaci�n.
 * 
 * Muestra un men� de opciones junto a la informaci�n del elemento actual.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class MenuActivity extends Activity implements OnClickListener {

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_menu);

		// Establecer escuchadores de clics
		this.findViewById(R.id.new_medicament_button_id).setOnClickListener(
				this);
		this.findViewById(R.id.exit_application_button_id).setOnClickListener(
				this);
	}

	@Override
	public void onResume() {
		super.onResume();
		this.findViewById(R.id.menu_progress_bar).setVisibility(View.VISIBLE);
		new CurrentProductFinder().execute();
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.new_medicament_button_id:
			goToAddProduct();
			break;
		case R.id.exit_application_button_id:
			exitApplication();
			break;
		}
	}

	/**
	 * Lanza la actividad que a�ade un nuevo producto.
	 */
	private void goToAddProduct() {
		CommonFunctions.startActivity(this, AddProductActivity.class);
	}

	/**
	 * Abandona la aplicaci�n.
	 */
	private void exitApplication() {
		finish();
		System.exit(0);
	}

	/**
	 * Realiza los c�lculos necesarios en segundo plano para mostrar el producto
	 * actual al usuario.
	 * 
	 * @author Cristian TG
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class CurrentProductFinder extends
			AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(final Void... params) {

			String text = null;

			final Product currentProduct = new ProductPersistence()
					.getCurrentProduct(MenuActivity.this);

			// Si no hay ning�n producto registrado
			if (currentProduct == null) {
				text = MenuActivity.this
						.getString(R.string.menu_current_product_no);

			} else {
				// Hay al menos un producto registrado
				final Consumption currentConsumption = getCurrentConsumption(currentProduct);
				final String name = currentProduct.getName();

				text = String
						.format(MenuActivity.this
								.getString(R.string.menu_current_product),
								currentProduct.getCode()
										+ (name != null ? '\n' + name : ""),
								currentConsumption == null ? MenuActivity.this
										.getString(R.string.menu_current_product_finished)
										: Consumption.dateFormat
												.format(currentConsumption
														.getDate().getTime()));

			}
			return text;
		}

		@Override
		protected void onPostExecute(final String text) {
			// Mostrar mensaje al usuario si se ha guardado el producto
			// correctamente
			MenuActivity.this.findViewById(R.id.menu_progress_bar)
					.setVisibility(View.GONE);
			final TextView currentData=(TextView) MenuActivity.this
					.findViewById(R.id.menu_current_data);
			if (text != null) {				
				currentData.setText(text);				
				currentData.setVisibility(View.VISIBLE);
			}else{
				currentData.setVisibility(View.GONE);
			}
		}

		/**
		 * Obtiene la siguiente fecha de toma del medicamento actual o null si
		 * no hay ninguna m�s.
		 * 
		 * @param currentProduct
		 *            Producto actual.
		 * @return Siguiente fecha de toma del medicamento actual o null si no
		 *         hay ninguna m�s.
		 */
		private final Consumption getCurrentConsumption(
				final Product currentProduct) {
			final ArrayList<Consumption> eventList = currentProduct.getEvents();
			Consumption currentConsumption = null;
			final Calendar now = Calendar.getInstance();
			Calendar auxCalendar;
			for (final Consumption consumption : eventList) {
				auxCalendar = consumption.getDate();
				// Si es candidato a ser la pr�xima toma
				if (now.before(auxCalendar)) {
					if (currentConsumption != null) {
						if (auxCalendar.before(currentConsumption)) {
							currentConsumption = consumption;
						}
					} else {
						currentConsumption = consumption;
					}
				}
			}
			return currentConsumption;
		}
	}
}