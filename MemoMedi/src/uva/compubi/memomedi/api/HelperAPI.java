package uva.compubi.memomedi.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Establece comunicaci�n mediante protocolo HTTP o HTTPS.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class HelperAPI {

	/**
	 * M�todo POST (http) para las llamadas.
	 */
	private final static String HTTP_POST = "POST";

	/**
	 * M�todo GET (http) para las llamadas.
	 */
	@SuppressWarnings("unused")
	private final static String HTTP_GET = "GET";

	/**
	 * Realiza una llamada HTTP (GET o POST).
	 * 
	 * @param url
	 *            URL dodne realizar la llamada.
	 * @param method
	 *            {@link HelperAPI#HTTP_POST} o {@link HelperAPI#HTTP_GET}
	 * @return Resultado de la llamada o null si hubo alg�n problema.
	 * @throws IllegalArgumentException
	 *             Si la url es nula, o el m�todo no es GET o POST.
	 */
	private static StringBuilder httpConnection(final String url,
			final String method) throws IllegalArgumentException {
		if (url == null) {
			throw new IllegalArgumentException("url can not be null.");
		}
		if (method.compareTo("GET") != 0 && method.compareTo("POST") != 0) {
			throw new IllegalArgumentException(
					"GET or POST method are only valid.");
		}
		final StringBuilder sb = new StringBuilder();
		try {
			final HttpURLConnection c = (HttpURLConnection) new URL(url)
					.openConnection();			
			c.setRequestMethod(method);
			c.connect();
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
		} catch (final MalformedURLException ex) {
			ex.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return sb;
	}

	/**
	 * Obtiene el nombre del producto (si existiera) de la API Rest:<br/>
	 * 
	 * <a href="http://www.outpan.com/">Outpan</a>
	 * 
	 * @param productCode
	 *            C�digo del proudtco a buscar.
	 * @return Nombre del producto buscado (puede estar vac�o).
	 */
	public static StringBuilder getNameAPI(final String productCode) {
		return httpConnection(
				"http://www.outpan.com/api/get-product.php?barcode="
						+ productCode
						+ "&apikey=411cfe9adcc0ba621dba4a01586e9516", HTTP_POST);
	}
}