/**
 * 
 * Contiene todo lo relacionado con las llamadas a la API de productos de la web <a href="http://www.outpan.com/</a>
 * 
 * Ver <a href="http://www.outpan.com/developers.php</a> para m�s detalles.
 *
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package uva.compubi.memomedi.api;