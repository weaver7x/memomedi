package uva.compubi.memomedi.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.annotation.SuppressLint;

/**
 * Modela un producto en la aplicaci�n que tiene asociado una serie de tomas
 * {@link Consumption}
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Product implements Serializable {

	/**
	 * UID para la serializaci�n.
	 */
	private static final long serialVersionUID = 2319264226783262267L;

	/**
	 * C�digo del producto.
	 */
	private String code;
	/**
	 * Nombre del producto (opcional).
	 */
	private String name;
	/**
	 * Uri de la imagen del producto (opcional).
	 */
	private String uri;
	/**
	 * Frecuencia (horas) de las tomas del producto.
	 */
	private int frequency;
	/**
	 * Fecha de comienzo de tomas del producto.
	 */
	private Calendar startDate;
	/**
	 * Identificador del calendario al que pertenece el producto.
	 */
	private long calendarId;
	/**
	 * Lista de eventos relacionados con el producto.
	 */
	private ArrayList<Consumption> events;
	/**
	 * �ndice (opcional) de la lista de eventos {@link Product#events} que
	 * indica el evento actual del producto.
	 */
	private int eventIndex;

	/**
	 * Formateador de fechas.
	 */
	@SuppressLint("SimpleDateFormat")
	public final static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yy HH:mm");

	/**
	 * Tama�o m�nimo del c�digo del producto.
	 */
	public static final int MIN_CODE_LENGTH = 1;

	/**
	 * Tama�o m�ximo del c�digo del producto.
	 */
	public static final int MAX_CODE_LENGTH = 500;

	/**
	 * Tama�o m�ximo del nombre opcional del producto.
	 */
	public static final byte MAX_NAME_LENGTH = 75;

	/**
	 * N�mero m�nimo de tomas.
	 */
	public static final byte MIN_CONSUMPTIONS_NUMBER = 1;

	/**
	 * N�mero m�ximo de tomas.
	 */
	public static final int MAX_CONSUMPTIONS_NUMBER = 9999;

	/**
	 * N�mero m�nimo de frecuencia de horas de tomas.
	 */
	public static final byte MIN_FREQUENCY_HOURS = 0;

	/**
	 * N�mero m�ximo de frecuencia de horas de tomas.
	 */
	public static final int MAX_FREQUENCY_HOURS = 9999;

	/**
	 * N�mero m�nimo de frecuencia de horas de tomas.
	 */
	public static final byte MIN_FREQUENCY_MIN = 0;

	/**
	 * N�mero m�ximo de frecuencia de horas de tomas.
	 */
	public static final int MAX_FREQUENCY_MIN = 59;

	/**
	 * N�mero m�nimo de frecuencia de minutos (totales) de tomas.
	 */
	public static final byte MIN_FREQUENCY = 1;

	/**
	 * �ndice por defecto de la lista de {@link Product#getEvents()} del evento
	 * actual del producto.
	 */
	public static final int DEFAULT_CURRENT_INDEX_EVENT = 0;

	/**
	 * Constructor sin indicar la lista de eventos del producto.
	 * 
	 * @param code
	 *            C�digo del producto.
	 * @param name
	 *            Nombre (opcional) del producto.
	 * @param uri
	 *            URI (opcional) del producto.
	 * @param frequency
	 *            Frecuencia (horas) de tomas del producto.
	 * @param startDate
	 *            Fecha de comienzo de las tomas del producto.
	 * @param calendarId
	 *            Identificador del calendario del producto.
	 * @throws IllegalArgumentException
	 *             Si alg�n par�metro fue incorrectamente especificado.
	 */
	public Product(final String code, final String name, final String uri,
			final int frequency, final Calendar startDate, final long calendarId)
			throws IllegalArgumentException {
		super();
		if (!rightCode(code)) {
			throw new IllegalArgumentException("invalid code");
		}
		if (!rightName(name)) {
			throw new IllegalArgumentException("invalid name");
		}
		if (!rightFrequency(frequency)) {
			throw new IllegalArgumentException("invalid frequency");
		}
		this.eventIndex = Product.DEFAULT_CURRENT_INDEX_EVENT;
		this.events = new ArrayList<Consumption>();
		this.code = code;
		this.name = name;
		this.uri = uri;
		this.frequency = frequency;
		this.startDate = startDate;
		this.calendarId = calendarId;
	}

	/**
	 * Constructor completo de un producto.
	 * 
	 * @param code
	 *            C�digo del producto.
	 * @param name
	 *            Nombre (opcional) del producto.
	 * @param uri
	 *            URI (opcional) del producto.
	 * @param frequency
	 *            Frecuencia (horas) de tomas del producto.
	 * @param startDate
	 *            Fecha de comienzo de las tomas del producto.
	 * @param calendarId
	 *            Identificador del calendario del producto.
	 * @param events
	 *            Lista de eventos relacionados con el producto.
	 * @param eventIndex
	 *            �ndice (opcional) de la lista de eventos.
	 * @throws IllegalArgumentException
	 *             Si alg�n par�metro fue incorrectamente especificado.
	 */
	public Product(final String code, final String name, final String uri,
			final int frequency, final Calendar startDate,
			final long calendarId, final ArrayList<Consumption> events,
			final int eventIndex) throws IllegalArgumentException {
		super();
		if (!rightCode(code)) {
			throw new IllegalArgumentException("invalid code");
		}
		if (!rightName(name)) {
			throw new IllegalArgumentException("invalid name");
		}
		if (!rightConsumptionsNumber(events)) {
			throw new IllegalArgumentException("invalid consumptionsNumber");
		}
		if (!rightFrequency(frequency)) {
			throw new IllegalArgumentException("invalid frequency");
		}
		this.events = events;
		if (!rightEventIndex(eventIndex)) {
			throw new IllegalArgumentException("invalid event index");
		}
		this.eventIndex = eventIndex;
		this.code = code;
		this.name = name;
		this.uri = uri;
		this.frequency = frequency;
		this.startDate = startDate;
		this.calendarId = calendarId;
	}

	/**
	 * Comprueba si el c�digo es correcto en base a las especificaciones.
	 * 
	 * @param code
	 *            C�digo a comprobar si es correcto.
	 * @return True si es correcto, False si no lo es.
	 */
	private static boolean rightCode(final String code) {
		final int LENTGH = code.length();
		return LENTGH >= MIN_CODE_LENGTH && LENTGH <= MAX_CODE_LENGTH;
	}

	/**
	 * Comprueba si el nombre es correcto en base a las especificaciones.
	 * 
	 * @param name
	 *            Nombre a comprobar si es correcto.
	 * @return True si es correcto, False si no lo es.
	 */
	private static boolean rightName(final String name) {
		final int LENTGH = name.length();
		return LENTGH <= MAX_NAME_LENGTH;
	}

	/**
	 * Comprueba si el n�mero de consumiciones es correcto en base a las
	 * especificaciones.
	 * 
	 * @param consumptionsNumber
	 *            N�mero de consumiciones a comprobar si es correcto.
	 * @return True si es correcto, False si no lo es.
	 */
	private static boolean rightConsumptionsNumber(ArrayList<Consumption> events) {
		if (events == null) {
			return false;
		}
		final int eventsSize = events.size();
		return eventsSize >= MIN_CONSUMPTIONS_NUMBER
				&& eventsSize <= MAX_CONSUMPTIONS_NUMBER;
	}

	/**
	 * Comprueba si la frecuencia (en minutos) de consumiciones es correcta en
	 * base a las especificaciones.
	 * 
	 * @param frequency
	 *            Frecuencia a comprobar si es correcta (en minutos).
	 * @return True si es correcto, False si no lo es.
	 */
	public static boolean rightFrequency(final int frequency) {
		return (frequency >= MIN_FREQUENCY)
				&& (frequency <= ((MAX_FREQUENCY_HOURS * 60) + MAX_FREQUENCY_MIN));
	}

	/**
	 * Comprueba si el �ndice indicado no se corresponde a la lista de eventos
	 * del producto.
	 * 
	 * @param frequency
	 *            Frecuencia a comprobar si es correcta.
	 * @return True si es correcto, False si no lo es.
	 */
	private boolean rightEventIndex(final int index) {
		return index >= DEFAULT_CURRENT_INDEX_EVENT
				&& index < this.events.size();
	}

	/**
	 * @return C�digo del producto.
	 */
	public final String getCode() {
		return this.code;
	}

	/**
	 * @param code
	 *            C�digo del producto.
	 * 
	 * @throws IllegalArgumentException
	 *             Si el c�digo fue indicado incorrectamente.
	 */
	public final void setCode(final String code)
			throws IllegalArgumentException {
		if (!rightCode(code)) {
			throw new IllegalArgumentException("invalid code");
		}
		this.code = code;
	}

	/**
	 * @return Nombre del producto (opcional).
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * @param name
	 *            Nombre del producto (opcional).
	 * @throws IllegalArgumentException
	 *             Si el nombre fue indicado incorrectamente.
	 */
	public final void setName(final String name)
			throws IllegalArgumentException {
		if (!rightName(name)) {
			throw new IllegalArgumentException("invalid name");
		}
		this.name = name;
	}

	/**
	 * @return Uri de la imagen del producto (opcional).
	 */
	public final String getUri() {
		return this.uri;
	}

	/**
	 * @param uri
	 *            Uri de la imagen del producto (opcional).
	 */
	public final void setUri(final String uri) {
		this.uri = uri;
	}

	/**
	 * 
	 * @return N�mero de tomas.
	 */
	public final int getConsumptionsNumber() {
		return this.events.size();
	}

	/**
	 * @return Frecuencia (horas) de las tomas del producto.
	 */
	public final int getFrequency() {
		return this.frequency;
	}

	/**
	 * @param frequency
	 *            Frecuencia (horas) de las tomas del producto.
	 * @throws IllegalArgumentException
	 *             Si la frecuencia fue indicado incorrectamente.
	 */
	public final void setFrequency(final int frequency)
			throws IllegalArgumentException {
		if (!rightFrequency(frequency)) {
			throw new IllegalArgumentException("invalid frequency");
		}
		this.frequency = frequency;
	}

	/**
	 * @return Fecha de comienzo de tomas del producto.
	 */
	public final Calendar getStartDate() {
		return this.startDate;
	}

	/**
	 * @param startDate
	 *            Fecha de comienzo de tomas del producto.
	 */
	public final void setStartDate(final Calendar startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return Identificador del calendario al que pertenece el producto.
	 */
	public long getCalendarId() {
		return this.calendarId;
	}

	/**
	 * @param calendarId
	 *            Identificador del calendario al que pertenece el producto.
	 */
	public void setCalendarId(long calendarId) {
		this.calendarId = calendarId;
	}

	/**
	 * @return the events
	 */
	public ArrayList<Consumption> getEvents() {
		return this.events;
	}

	/**
	 * @param events
	 *            Lista de eventos (tomas) relacionados con el producto.
	 */
	public void setEvents(final ArrayList<Consumption> events) {
		if (!rightConsumptionsNumber(events)) {
			throw new IllegalArgumentException("invalid consumptionsNumber");
		}
		this.events = events;
	}

	/**
	 * @return �ndice (opcional) de la lista de eventos
	 *         {@link Product#getEvents()} que indica el evento actual del
	 *         producto.
	 */
	public int getEventIndex() {
		return this.eventIndex;
	}

	/**
	 * @param eventIndex
	 *            �ndice (opcional) de la lista de eventos
	 *            {@link Product#getEvents()} que indica el evento actual del
	 *            producto.
	 */
	public void setEventIndex(final int eventIndex) {
		if (!rightEventIndex(eventIndex)) {
			throw new IllegalArgumentException("invalid event index");
		}
		this.eventIndex = eventIndex;
	}

}