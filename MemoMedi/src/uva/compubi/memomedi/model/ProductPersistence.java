package uva.compubi.memomedi.model;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

/**
 * Se encarga de todas las operaciones persistentes y de acceso a datos de la
 * aplicaci�n.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ProductPersistence {

	/**
	 * Guarda un producto en la persistencia del dispositivo (sin modificar los
	 * que hubiera anteriormente).
	 * 
	 * @param context
	 *            Contexto de la llamada.
	 * @param product
	 *            Producto a a�adir.
	 * @return True si todo fue correctamente, False si no se pudo guardar.
	 */
	public boolean saveProduct(final Context context, final Product product) {
		try {

			// 1. Obtener los posibles productos anteriores
			JSONArray productsJSONArray = this.getAllProductsJSON(context);
			if (productsJSONArray == null) {
				productsJSONArray = new JSONArray();
			}

			// 2. A�adir el nuevo producto al final de la lista
			final JSONObject mProduct = new JSONObject();
			mProduct.put(JSON_CODE, product.getCode());
			mProduct.put(JSON_NAME, product.getName());
			mProduct.put(JSON_URI, product.getUri());
			mProduct.put(JSON_FREQUENCY, product.getFrequency());
			mProduct.put(JSON_START_DATE, Product.dateFormat.format((product
					.getStartDate().getTime())));
			mProduct.put(JSON_CALENDAR_ID, product.getCalendarId());

			final JSONArray eventsJSONArray = new JSONArray();
			final ArrayList<Consumption> events = product.getEvents();
			JSONObject mEventAux;
			for (final Consumption consumption : events) {
				mEventAux = new JSONObject();
				mEventAux.put(JSON_CONSUMPTION_EVENT_ID,
						consumption.getEventID());
				mEventAux.put(JSON_CONSUMPTION_DONE, consumption.isDone());
				mEventAux.put(JSON_CONSUMPTION_DATE, Consumption.dateFormat
						.format((consumption.getDate().getTime())));
				mEventAux.put(JSON_CONSUMPTION_NUMBER,
						consumption.getConsumptionNumber());
				eventsJSONArray.put(mEventAux);
			}

			productsJSONArray.put(mProduct.put(JSON_CONSUMPTION,
					eventsJSONArray));

			// 3. Guardar todos los productos
			this.writeInternalMemory(context, productsJSONArray.toString());
			return true;
		} catch (final Exception e) {

		}
		return false;
	}

	/**
	 * Devuelve el producto actual en la persistencia que est� siendo utilizado
	 * o null si no hay ninguno.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * 
	 * @return Producto actual en la persistencia que est� siendo utilizado o
	 *         null si no hay ninguno.
	 */
	public Product getCurrentProduct(final Context context) {
		// 1. Obtener los productos
		JSONArray productsJSONArray = this.getAllProductsJSON(context);
		if (productsJSONArray == null) {
			productsJSONArray = new JSONArray();
		}

		final int productsLength = productsJSONArray.length();
		if (productsLength > 0) {
			try {
				final JSONObject currentJSON = productsJSONArray
						.getJSONObject(productsLength - 1);
				final ArrayList<Consumption> events = new ArrayList<Consumption>();
				final JSONArray eventsJSONArray = currentJSON
						.getJSONArray(JSON_CONSUMPTION);
				final int eventsLength = eventsJSONArray.length();
				JSONObject mJSONObject;
				Calendar calendar;
				final String code = currentJSON.getString(JSON_CODE);
				for (int i = 0; i < eventsLength; i++) {
					calendar = Calendar.getInstance();
					mJSONObject = eventsJSONArray.getJSONObject(i);
					calendar.setTime(Consumption.dateFormat.parse(mJSONObject
							.getString(JSON_CONSUMPTION_DATE)));
					events.add(new Consumption(code, mJSONObject
							.getLong(JSON_CONSUMPTION_EVENT_ID), mJSONObject
							.getBoolean(JSON_CONSUMPTION_DONE), calendar,
							mJSONObject.getInt(JSON_CONSUMPTION_NUMBER)));
				}
				calendar = Calendar.getInstance();
				calendar.setTime(Product.dateFormat.parse(currentJSON
						.getString(JSON_START_DATE)));
				return new Product(code, currentJSON.getString(JSON_NAME),
						currentJSON.getString(JSON_URI),
						currentJSON.getInt(JSON_FREQUENCY), calendar,
						currentJSON.getLong(JSON_CALENDAR_ID), events,
						Product.DEFAULT_CURRENT_INDEX_EVENT);

			} catch (final Exception ex) {

			}
		}

		return null;
	}

	/**
	 * 
	 * Obtiene la lista de productos de la aplicaci�n.
	 * 
	 * @param context
	 *            Contexto de uso de la aplicaci�n.
	 * @return Lista de productos registrado en la persistencia o null si hubo
	 *         alg�n problema.
	 */
	public ArrayList<Product> getAllProducts(final Context context) {
		final ArrayList<Product> products = new ArrayList<Product>();
		ArrayList<Consumption> events;
		JSONObject mProduct, mEvent;
		JSONArray mEvents;
		int consumptionLength;
		String code;
		Calendar mCalendar;
		try {
			final JSONArray productsJSON = getAllProductsJSON(context);
			final int totalProducts = productsJSON.length();
			for (int i = 0; i < totalProducts; i++) {

				mProduct = productsJSON.getJSONObject(i);
				code = mProduct.getString(JSON_CODE);
				mEvents = mProduct.getJSONArray(JSON_CONSUMPTION);
				consumptionLength = mEvents.length();
				events = new ArrayList<Consumption>();
				for (int j = 0; j < consumptionLength; j++) {
					mEvent = mEvents.getJSONObject(j);
					mCalendar = Calendar.getInstance();
					mCalendar.setTime(Consumption.dateFormat.parse(mEvent
							.getString(JSON_CONSUMPTION_DATE)));
					events.add(new Consumption(code, mEvent
							.getLong(JSON_CONSUMPTION_EVENT_ID), mEvent
							.getBoolean(JSON_CONSUMPTION_DONE), mCalendar,
							mEvent.getInt(JSON_CONSUMPTION_NUMBER)));
				}
				mCalendar = Calendar.getInstance();
				mCalendar.setTime(Product.dateFormat.parse(mProduct
						.getString(JSON_START_DATE)));
				products.add(new Product(code, mProduct.getString(JSON_NAME),
						mProduct.getString(JSON_URI), mProduct
								.getInt(JSON_FREQUENCY), mCalendar, mProduct
								.getLong(JSON_CALENDAR_ID), events,
						Product.DEFAULT_CURRENT_INDEX_EVENT));
			}
			return products;
		} catch (final Exception ex) {
			return null;
		}
	}

	/**
	 * Obtiene todos los productos de la persistencia en formato JSON.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @return JSONArray de los productos.
	 */
	private final JSONArray getAllProductsJSON(final Context context) {
		try {
			// 1. Obtener los productos actuales
			final String jsonData = readInternalMemory(context);
			JSONArray productsJSONArray;
			if (jsonData == null || jsonData.length() == 0) {
				productsJSONArray = new JSONArray();
			} else {
				productsJSONArray = new JSONArray(jsonData);
			}
			return productsJSONArray;
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 * Nombre del fichcero JSON que contiene toda la informaci�n de los eventos
	 * de los productos a�adidos en la persistencia.
	 */
	public final static String PERSISTENCE_FILE_NAME = "events.json";

	/**
	 * Clave JSON para el c�digo del producto.
	 */
	private final static String JSON_CODE = "code";
	/**
	 * Clave JSON para el nombre del producto.
	 */
	private final static String JSON_NAME = "name";
	/**
	 * Clave JSON para la URI del producto.
	 */
	private final static String JSON_URI = "uri";
	/**
	 * Clave JSON para la frecuencia de tomas del producto.
	 */
	private final static String JSON_FREQUENCY = "frequency";
	/**
	 * Clave JSON para la fecha de comienzo del producto.
	 */
	private final static String JSON_START_DATE = "startDate";
	/**
	 * Clave JSON para el ID del calendario de las tomas del producto.
	 */
	private final static String JSON_CALENDAR_ID = "calendarId";
	/**
	 * Clave JSON para el c�digo del evento del producto.
	 */
	private final static String JSON_CONSUMPTION = "consumption";
	/**
	 * Clave JSON para el c�digo del evento del producto.
	 */
	private final static String JSON_CONSUMPTION_EVENT_ID = "eventId";
	/**
	 * Clave JSON para indicar si se ha realizado la toma del producto.
	 */
	private final static String JSON_CONSUMPTION_DONE = "done";
	/**
	 * Clave JSON para indicar la fecha del evento del producto.
	 */
	private final static String JSON_CONSUMPTION_DATE = "eventDate";
	/**
	 * Clave JSON para indicar la posici�n del evento entre la lisat de eventos
	 * del producto.
	 */
	private final static String JSON_CONSUMPTION_NUMBER = "number";

	/**
	 * Guarda un evento como "realizado" en la persistencia.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param code
	 *            C�digo del producto al que pertenece el evento.
	 * @param calendarId
	 *            Identificador del calendario al que pertenece el evento.
	 * @param eventId
	 *            Identificaro del evento a establecer a "realizado".
	 * @return True si todo fue bien, False en caso contrario.
	 */
	public boolean saveEventDone(final Context context, final String code,
			final long calendarId, final long eventId) {
		final JSONArray products = this.getAllProductsJSON(context);
		try {
			boolean found = false;
			final int productsSize = products.length();
			int consumptionsSize;
			JSONObject product, mConsumption;
			JSONArray consumptions;
			for (int i = 0; !found && i < productsSize; i++) {
				product = products.getJSONObject(i);
				if (product.getLong(JSON_CALENDAR_ID) == calendarId
						&& product.getString(JSON_CODE).equals(code)) {
					consumptions = product.getJSONArray(JSON_CONSUMPTION);
					consumptionsSize = consumptions.length();
					for (int j = 0; j < consumptionsSize; j++) {
						mConsumption = consumptions.getJSONObject(j);
						if (mConsumption.getLong(JSON_CONSUMPTION_EVENT_ID) == eventId) {
							mConsumption.put(JSON_CONSUMPTION_DONE, true);
							found = true;
						}
					}
				}
			}
			this.writeInternalMemory(context, products.toString());
			return true;
		} catch (final Exception e) {
			return false;
		}
	}

	/**
	 * Guarda los datos al final del fichero de la memoria interna:
	 * data/data/uva.compubi.memomedi/files/<
	 * {@link ProductPersistence#PERSISTENCE_FILE_NAME}>
	 * 
	 * @param context
	 *            Contexto de uso
	 * @param content
	 *            Contenido a guardar en el fichero.
	 */
	public synchronized void writeInternalMemory(final Context context,
			final String content) {
		try {
			final FileOutputStream fos = context
					.getApplicationContext()
					.openFileOutput(PERSISTENCE_FILE_NAME, Context.MODE_PRIVATE);
			fos.write(content.getBytes());
			fos.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lee los datos del fichero de la memoria interna:
	 * data/data/uva.compubi.memomedi/files/<
	 * {@link ProductPersistence#PERSISTENCE_FILE_NAME}>
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @return Contenido del fichero indicado.
	 */
	private String readInternalMemory(final Context context) {
		try {
			final BufferedReader inputReader = new BufferedReader(
					new InputStreamReader(context.getApplicationContext()
							.openFileInput(PERSISTENCE_FILE_NAME)));
			String inputString;
			final StringBuilder mStringBuilder = new StringBuilder();
			while ((inputString = inputReader.readLine()) != null) {
				mStringBuilder.append(inputString + '\n');
			}
			inputReader.close();
			return mStringBuilder.toString();
		} catch (final Exception ex) {
			// En caso de que no exista no se hace nada.
		}
		return "";
	}
}