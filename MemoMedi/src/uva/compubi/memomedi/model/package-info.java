/**
 * 
 * Contiene los modelos de datos y los m�todos de acceso y de persistencia a los mismos.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package uva.compubi.memomedi.model;