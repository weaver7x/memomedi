package uva.compubi.memomedi.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;

/**
 * Modela una consumici�n de un producto en la aplicaci�n.
 * 
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Consumption implements Serializable {

	/**
	 * UID para la serializaci�n.
	 */
	private static final long serialVersionUID = -925745112400061589L;
	/**
	 * C�digo del producto asociado.
	 */
	private String productCode;
	/**
	 * Identificador del evento en el calendario del sistema.
	 */
	private long eventID;
	/**
	 * Bandera que indica si est� hecho o no.
	 */
	private boolean done;
	/**
	 * Fecha del evento.
	 */
	private Calendar date;
	/**
	 * N�mero de toma.
	 */
	private int consumptionNumber;

	/**
	 * Formateador de fechas.
	 */
	@SuppressLint("SimpleDateFormat")
	public final static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yy HH:mm");

	/**
	 * No se permiten creaciones sin par�metros.
	 */
	@SuppressWarnings("unused")
	private Consumption() {

	}

	/**
	 * Constructor de
	 * 
	 * @param productCode
	 *            C�digo del producto asociado.
	 * @param eventID
	 *            Identificador del evento en el calendario del sistema.
	 * @param done
	 *            Bandera que indica si est� hecho o no.
	 * @param date
	 *            Fecha del evento.
	 * @param consumptionNumber
	 *            N�mero de toma.
	 */
	public Consumption(final String productCode, final long eventID,
			final boolean done, final Calendar date, final int consumptionNumber) {
		this.productCode = productCode;
		this.eventID = eventID;
		this.done = done;
		this.date = date;
		this.consumptionNumber = consumptionNumber;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return this.productCode;
	}

	/**
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(final String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the eventID
	 */
	public long getEventID() {
		return this.eventID;
	}

	/**
	 * @param eventID
	 *            the eventID to set
	 */
	public void setEventID(final long eventID) {
		this.eventID = eventID;
	}

	/**
	 * @return the done
	 */
	public boolean isDone() {
		return this.done;
	}

	/**
	 * @param done
	 *            the done to set
	 */
	public void setDone(final boolean done) {
		this.done = done;
	}

	/**
	 * @return the date
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(final Calendar date) {
		this.date = date;
	}

	/**
	 * @return the consumptionNumber
	 */
	public int getConsumptionNumber() {
		return this.consumptionNumber;
	}

	/**
	 * @param consumptionNumber
	 *            the consumptionNumber to set
	 */
	public void setConsumptionNumber(final int consumptionNumber) {
		this.consumptionNumber = consumptionNumber;
	}
}