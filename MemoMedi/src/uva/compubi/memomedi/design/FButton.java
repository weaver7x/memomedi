package uva.compubi.memomedi.design;

import uva.compubi.memomedi.R;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * 
 * Modela un bot�n estilo de API 5.0 para versiones anteriores.<br/>
 * 
 * Ver <a target="_blank" href="https://github.com/hoang8f/android-flat-button">hoang8f flat button</a><br/>
 * 
 * Created by hoang8f on 5/5/14.<br/>
 * 
 * Modificado por Cristian TG para adapatarlo a la aplicaci�n.
 * 
 * @author hoang8f
 * @author Cristian TG
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
@SuppressLint("ClickableViewAccessibility")
public final class FButton extends Button implements View.OnTouchListener {

	// Custom values
	private boolean isShadowEnabled = true;
	private int mButtonColor;
	private int mShadowColor;
	private int mShadowHeight;
	private int mCornerRadius;
	// Native values
	private int mPaddingLeft;
	private int mPaddingRight;
	private int mPaddingTop;
	private int mPaddingBottom;
	// Background drawable
	private Drawable pressedDrawable;
	private Drawable unpressedDrawable;

	/**
	 * Indica sila sombra est� establecida.
	 */
	boolean isShadowColorDefined = false;

	/**
	 * Constructor con contexto.
	 * 
	 * @param context
	 *            Contexto de uso.
	 */
	public FButton(final Context context) {
		super(context);
		init();
		this.setOnTouchListener(this);
	}

	/**
	 * Constructor con contexto y atributos.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param attrs
	 *            Atributos.
	 */
	public FButton(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init();
		parseAttrs(context, attrs);
		this.setOnTouchListener(this);
	}

	/**
	 * Constructor con contexto, atributos y estilo.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param attrs
	 *            Atributos.
	 * @param defStyle
	 *            Estilo.
	 */
	public FButton(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		init();
		parseAttrs(context, attrs);
		this.setOnTouchListener(this);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		// Update background color
		refresh();
	}

	@Override
	public boolean onTouch(final View view, final MotionEvent motionEvent) {
		switch (motionEvent.getAction()) {
		case MotionEvent.ACTION_DOWN:
			updateBackground(this.pressedDrawable);
			this.setPadding(this.mPaddingLeft, this.mPaddingTop
					+ this.mShadowHeight, this.mPaddingRight,
					this.mPaddingBottom);
			break;
		case MotionEvent.ACTION_MOVE:
			final Rect r = new Rect();
			view.getLocalVisibleRect(r);
			if (!r.contains((int) motionEvent.getX(), (int) motionEvent.getY()
					+ 3 * this.mShadowHeight)
					&& !r.contains((int) motionEvent.getX(),
							(int) motionEvent.getY() - 3 * this.mShadowHeight)) {
				updateBackground(this.unpressedDrawable);
				this.setPadding(this.mPaddingLeft, this.mPaddingTop
						+ this.mShadowHeight, this.mPaddingRight,
						this.mPaddingBottom + this.mShadowHeight);
			}
			break;
		case MotionEvent.ACTION_OUTSIDE:
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			updateBackground(this.unpressedDrawable);
			this.setPadding(this.mPaddingLeft, this.mPaddingTop
					+ this.mShadowHeight, this.mPaddingRight,
					this.mPaddingBottom + this.mShadowHeight);
			break;
		}
		return false;
	}

	/**
	 * Inicializar a valores iniciales.
	 */
	private void init() {
		this.isShadowEnabled = true;
		final Resources resources = getResources();
		if (resources == null) {
			return;
		}
		this.mButtonColor = resources.getColor(R.color.fbutton_default_color);
		this.mShadowColor = resources
				.getColor(R.color.fbutton_default_shadow_color);
		this.mShadowHeight = resources
				.getDimensionPixelSize(R.dimen.fbutton_default_shadow_height);
		this.mCornerRadius = resources
				.getDimensionPixelSize(R.dimen.fbutton_default_conner_radius);
	}

	/**
	 * Parsea los atributos.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param attrs
	 *            Atributos a parsear.
	 */
	private void parseAttrs(final Context context, final AttributeSet attrs) {
		// Load from custom attributes
		TypedArray typedArray = context.obtainStyledAttributes(attrs,
				R.styleable.FButton);
		if (typedArray == null)
			return;
		for (int i = 0; i < typedArray.getIndexCount(); i++) {
			int attr = typedArray.getIndex(i);
			if (attr == R.styleable.FButton_shadowEnabled) {
				this.isShadowEnabled = typedArray.getBoolean(attr, true); // Default
																			// is
																			// true
			} else if (attr == R.styleable.FButton_buttonColor) {
				this.mButtonColor = typedArray.getColor(attr,
						R.color.fbutton_default_color);
			} else if (attr == R.styleable.FButton_shadowColor) {
				this.mShadowColor = typedArray.getColor(attr,
						R.color.fbutton_default_shadow_color);
				this.isShadowColorDefined = true;
			} else if (attr == R.styleable.FButton_shadowHeight) {
				this.mShadowHeight = typedArray.getDimensionPixelSize(attr,
						R.dimen.fbutton_default_shadow_height);
			} else if (attr == R.styleable.FButton_cornerRadius) {
				this.mCornerRadius = typedArray.getDimensionPixelSize(attr,
						R.dimen.fbutton_default_conner_radius);
			}
		}
		typedArray.recycle();

		// Get paddingLeft, paddingRight
		int[] attrsArray = new int[] { android.R.attr.paddingLeft, // 0
				android.R.attr.paddingRight, // 1
		};
		TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
		if (ta == null)
			return;
		this.mPaddingLeft = ta.getDimensionPixelSize(0, 0);
		this.mPaddingRight = ta.getDimensionPixelSize(1, 0);
		ta.recycle();

		// Get paddingTop, paddingBottom
		int[] attrsArray2 = new int[] { android.R.attr.paddingTop, // 0
				android.R.attr.paddingBottom,// 1
		};
		final TypedArray ta1 = context.obtainStyledAttributes(attrs,
				attrsArray2);
		if (ta1 == null)
			return;
		this.mPaddingTop = ta1.getDimensionPixelSize(0, 0);
		this.mPaddingBottom = ta1.getDimensionPixelSize(1, 0);
		ta1.recycle();
	}

	/**
	 * Refresca la vista del bot�n.
	 */
	@SuppressLint("NewApi")
	public void refresh() {
		int alpha = Color.alpha(this.mButtonColor);
		float[] hsv = new float[3];
		Color.colorToHSV(this.mButtonColor, hsv);
		hsv[2] *= 0.8f; // value component
		// if shadow color was not defined, generate shadow color = 80%
		// brightness
		if (!this.isShadowColorDefined) {
			this.mShadowColor = Color.HSVToColor(alpha, hsv);
		}
		// Create pressed background and unpressed background drawables

		if (this.isEnabled()) {
			if (this.isShadowEnabled) {
				this.pressedDrawable = createDrawable(this.mCornerRadius,
						Color.TRANSPARENT, this.mButtonColor);
				this.unpressedDrawable = createDrawable(this.mCornerRadius,
						this.mButtonColor, this.mShadowColor);
			} else {
				this.mShadowHeight = 0;
				this.pressedDrawable = createDrawable(this.mCornerRadius,
						this.mShadowColor, Color.TRANSPARENT);
				this.unpressedDrawable = createDrawable(this.mCornerRadius,
						this.mButtonColor, Color.TRANSPARENT);
			}
		} else {
			Color.colorToHSV(this.mButtonColor, hsv);
			hsv[1] *= 0.25f; // saturation component
			int disabledColor = this.mShadowColor = Color
					.HSVToColor(alpha, hsv);
			// Disabled button does not have shadow
			this.pressedDrawable = createDrawable(this.mCornerRadius,
					disabledColor, Color.TRANSPARENT);
			this.unpressedDrawable = createDrawable(this.mCornerRadius,
					disabledColor, Color.TRANSPARENT);
		}
		updateBackground(this.unpressedDrawable);
		// Set padding
		this.setPadding(this.mPaddingLeft, this.mPaddingTop
				+ this.mShadowHeight, this.mPaddingRight, this.mPaddingBottom
				+ this.mShadowHeight);
	}

	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void updateBackground(final Drawable background) {
		if (background == null)
			return;
		// Para que funcione en versiones anteriores.
		// Set button background
		this.setBackgroundDrawable(background);
		// if (Build.VERSION.SDK_INT >= 16) {
		// this.setBackgroundDrawable(background);
		// } else {
		// this.setBackgroundDrawable(background);
		// }
	}

	private LayerDrawable createDrawable(final int radius, final int topColor,
			final int bottomColor) {

		final float[] outerRadius = new float[] { radius, radius, radius,
				radius, radius, radius, radius, radius };

		// Top
		final RoundRectShape topRoundRect = new RoundRectShape(outerRadius,
				null, null);
		final ShapeDrawable topShapeDrawable = new ShapeDrawable(topRoundRect);
		topShapeDrawable.getPaint().setColor(topColor);
		// Bottom
		final RoundRectShape roundRectShape = new RoundRectShape(outerRadius,
				null, null);
		final ShapeDrawable bottomShapeDrawable = new ShapeDrawable(
				roundRectShape);
		bottomShapeDrawable.getPaint().setColor(bottomColor);
		// Create array
		Drawable[] drawArray = { bottomShapeDrawable, topShapeDrawable };
		LayerDrawable layerDrawable = new LayerDrawable(drawArray);

		// Set shadow height
		if (this.isShadowEnabled && topColor != Color.TRANSPARENT) {
			// unpressed drawable
			layerDrawable.setLayerInset(0, 0, 0, 0, 0); /*
														 * index, left, top,
														 * right, bottom
														 */
		} else {
			// pressed drawable
			layerDrawable.setLayerInset(0, 0, this.mShadowHeight, 0, 0); /*
																		 * index,
																		 * left,
																		 * top,
																		 * right
																		 * ,
																		 * bottom
																		 */
		}
		layerDrawable.setLayerInset(1, 0, 0, 0, this.mShadowHeight); /*
																	 * index,
																	 * left,
																	 * top,
																	 * right,
																	 * bottom
																	 */

		return layerDrawable;
	}

	/**
	 * Establece si la sombra est� establecida.
	 * 
	 * @param isShadowEnabled
	 *            True si la sombra est� establecida, False en caso contrario.
	 */
	public void setShadowEnabled(final boolean isShadowEnabled) {
		this.isShadowEnabled = isShadowEnabled;
		setShadowHeight(0);
		refresh();
	}

	/**
	 * Establece el color del bot�n.
	 * 
	 * @param buttonColor
	 *            Color del bot�n a establecer.
	 */
	public void setButtonColor(final int buttonColor) {
		this.mButtonColor = buttonColor;
		refresh();
	}

	/**
	 * Establece el color de la sombra.
	 * 
	 * @param shadowColor
	 *            Color de la sombra.
	 */
	public void setShadowColor(final int shadowColor) {
		this.mShadowColor = shadowColor;
		this.isShadowColorDefined = true;
		refresh();
	}

	/**
	 * Establece el tama�o de la sombra.
	 * 
	 * @param shadowHeight
	 *            Tama�o de la sombra.
	 */
	public void setShadowHeight(final int shadowHeight) {
		this.mShadowHeight = shadowHeight;
		refresh();
	}

	/**
	 * Establece el tama�o del borde.
	 * 
	 * @param cornerRadius
	 *            Tama�o del borde.
	 */
	public void setCornerRadius(final int cornerRadius) {
		this.mCornerRadius = cornerRadius;
		refresh();
	}

	/**
	 * Establece el padding del bot�n.
	 * 
	 * @param left
	 *            Padding izquierdo.
	 * @param top
	 *            Padding superior.
	 * @param right
	 *            Padding derecho.
	 * @param bottom
	 *            Padding infieror.
	 */
	public void setFButtonPadding(final int left, final int top,
			final int right, final int bottom) {
		this.mPaddingLeft = left;
		this.mPaddingRight = right;
		this.mPaddingTop = top;
		this.mPaddingBottom = bottom;
		refresh();
	}

	@Override
	public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);
		refresh();
	}

	/**
	 * Indica si est� establecida la sombra.
	 * 
	 * @return Si est� establecida la sombra.
	 */
	public boolean isShadowEnabled() {
		return this.isShadowEnabled;
	}

	/**
	 * Obtiene el color del bot�n.
	 * 
	 * @return Color del bot�n.
	 */
	public int getButtonColor() {
		return this.mButtonColor;
	}

	/**
	 * Obtiene el color de la sombra.
	 * 
	 * @return Color de la sombra.
	 */
	public int getShadowColor() {
		return this.mShadowColor;
	}

	/**
	 * Obtiene el tama�o de la sombra.
	 * 
	 * @return Tama�o de la sombra.
	 */
	public int getShadowHeight() {
		return this.mShadowHeight;
	}

	/**
	 * Obtiene la esquina del borde.
	 * 
	 * @return Esquina del borde.
	 */
	public int getCornerRadius() {
		return this.mCornerRadius;
	}
}